<?php

namespace YOOtheme\Builder\Wordpress\Source\Type;

use YOOtheme\Builder\Source;
use YOOtheme\Builder\Wordpress\Source\AcfHelper;
use YOOtheme\Str;

class FieldsType
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Source
     */
    protected $source;

    /**
     * Constructor.
     *
     * @param string $type
     * @param string $name
     */
    public function __construct($type, $name = '')
    {
        $this->type = $type;
        $this->name = $name;
    }

    public function __invoke(Source $source)
    {
        $this->source = $source;

        $fields = [];
        $resolvers = [];

        foreach (AcfHelper::groups($this->type, $this->name) as $group) {
            foreach (acf_get_fields($group) as $field) {

                if (in_array($field['type'], ['message', 'accordion', 'tab', 'clone', 'flexible_content'])) {
                    continue;
                }

                $config = [
                    'type' => 'String',
                    'metadata' => [
                        'label' => $field['label'] ?: $field['name'],
                        'group' => $group['title'],
                    ],
                ];

                $field['group'] = $group['title'];

                $fieldType = Str::camelCase($field['type'], true);
                $fieldConfig = is_callable($load = [$this, "loadField{$fieldType}"]) ? $load($field, $config) : $this->loadField($field, $config);

                if (!$fieldConfig) {
                    continue;
                }

                $fields[$field['name']] = $fieldConfig;
                $resolvers[$field['name']] = function ($item) use ($field, $fieldType) {
                    return is_callable($resolve = [$this, "resolveField{$fieldType}"]) ? $resolve($field, $item) : $this->resolveField($field, $item);
                };

            }
        }

        return compact('fields', 'resolvers');
    }

    protected function loadField($field, array $config)
    {
        if (isset($field['choices'])) {
            return $this->loadFieldChoices($field, $config);
        }

        if (in_array($field['type'], ['file', 'image'])) {
            return $this->loadFieldAttachment($field, $config);
        }

        if (in_array($field['type'], ['time_picker', 'date_time_picker'])) {
            return $this->loadFieldDatePicker($field, $config);
        }

        if (in_array($field['type'], ['relationship'])) {
            return $this->loadFieldPostObject($field, $config);
        }

        if (isset($field['sub_fields'])) {
            return $this->loadFieldSubfields($field, $config);
        }

        if (in_array($field['type'], ['text', 'text_area', 'wysiwyg'])) {
            $config = array_merge_recursive(
                $config,
                ['metadata' => ['filters' => ['limit']]]
            );
        }

        if ($this->isMultiple($field)) {
            return ['type' => ['listOf' => 'ValueField']] + $config;
        }

        return $config;
    }

    protected function loadFieldDatePicker($field, array $config)
    {
        return array_merge_recursive(
            $config,
            ['metadata' => ['filters' => ['date']]]
        );
    }

    protected function loadFieldPostObject($field, $config)
    {
        if (empty($field['post_type']) || count($field['post_type']) > 1) {
            return;
        }

        $type = $this->getPostType(array_pop($field['post_type']));

        if (!$type) {
            return;
        }

        $type = Str::camelCase($type->name, true);

        return ['type' => $this->isMultiple($field) ? ['listOf' => $type] : $type] + $config;
    }

    protected function loadFieldTaxonomy($field, $config)
    {
        $taxonomy = !empty($field['taxonomy'])
            ? $this->getTaxonomy($field['taxonomy'])
            : false;

        if (!$taxonomy) {
            return;
        }

        $taxonomy = Str::camelCase($taxonomy->name, true);

        return ['type' => $this->isMultiple($field) ? ['listOf' => $taxonomy] : $taxonomy] + $config;
    }

    protected function loadFieldUser($field, array $config)
    {
        return ['type' => $this->isMultiple($field) ? ['listOf' => 'User'] : 'User'] + $config;
    }

    protected function loadFieldChoices($field, array $config)
    {
        return ['type' => $this->isMultiple($field) ? ['listOf' => 'ChoiceField'] : 'ChoiceField'] + $config;
    }

    protected function loadFieldLink($field, array $config)
    {
        return ['type' => 'LinkField'] + $config;
    }

    protected function loadFieldGoogleMap($field, array $config)
    {
        return ['type' => 'GoogleMapsField'] + $config;
    }

    protected function loadFieldAttachment($field, array $config)
    {
        return ['type' => 'Attachment'] + $config;
    }

    protected function loadFieldGallery($field, array $config)
    {
        return ['type' => ['listOf' => 'Attachment']] + $config;
    }

    protected function loadFieldSubfields($field, array $config)
    {
        $fields = [];
        foreach ($field['sub_fields'] as $sub_field) {
            $fields[$sub_field['name']] = $this->loadField($sub_field, [
                'type' => 'String',
                'metadata' => [
                    'label' => $sub_field['label'] ?: $sub_field['name'],
                    'group' => $field['label'] ?: $field['name'],
                ],
            ]);
        }

        $name = Str::camelCase(['Field', $field['name']], true);

        $this->source->addType($name, FieldType::class, ['fields' => $fields]);

        return ['type' => $this->isMultiple($field) ? ['listOf' => $name] : $name] + $config;
    }

    protected function resolveField($field, $item)
    {
        $postId = acf_get_valid_post_id($item);

        // Subfields field
        if (array_key_exists('sub_fields', $field)) {

            if (empty($field['sub_fields'])) {
                return;
            }

            if ($this->isMultiple($field)) {

                $value = acf_get_metadata($postId, $field['name']);

                $values = [];
                for ($i = 0; $i < $value; $i++) {
                    foreach ($field['sub_fields'] as $subfield) {
                        $values[$i][$subfield['name']] = $this->resolveField(['name' => "{$field['name']}_{$i}_{$subfield['name']}"] + $subfield, $item);
                    }
                }

                return $values;
            }

            $values = [];
            foreach ($field['sub_fields'] as $subfield) {
                $values[$subfield['name']] = $this->resolveField($subfield, $item);
            }

            return $values;
        }

        switch ($field['type']) {
            case 'post_object':
            case 'relationship':
            case 'taxonomy':
            case 'user':
                $field['return_format'] = 'object';
                break;
            case 'button_group':
            case 'checkbox':
            case 'radio':
            case 'select':
            case 'link':
                $field['return_format'] = 'array';
                break;
            case 'file':
            case 'gallery':
            case 'image':
            $field['return_format'] = 'id';
        }

        // get value for field
        $value = acf_get_value($postId, $field);

        if ($value === null) {
            return;
        }

        $value = acf_format_value($value, $postId, $field);

        if (!empty($field['return_format'])) {
            return $value ?: null;
        }

        if ($this->isMultiple($field)) {
            return array_map(function ($value) { return compact('value'); }, $value);
        }

        return $value;
    }

    protected function isMultiple($field)
    {
        return !empty($field['multiple']) && $field['multiple']
            || in_array($field['type'], ['checkbox', 'relationship'])
            || !empty($field['field_type']) && !in_array($field['field_type'], ['select', 'radio'])
            || isset($field['sub_fields'], $field['max']);
    }

    protected function getPostType($post_type)
    {
        global $wp_post_types;

        if (empty($wp_post_types[$post_type]->rest_base) || $wp_post_types[$post_type]->name === $wp_post_types[$post_type]->rest_base) {
            return;
        }

        return $wp_post_types[$post_type];
    }

    protected function getTaxonomy($taxonomy)
    {
        global $wp_taxonomies;

        if (empty($wp_taxonomies[$taxonomy]->rest_base) || $wp_taxonomies[$taxonomy]->name === $wp_taxonomies[$taxonomy]->rest_base) {
            return;
        }

        return $wp_taxonomies[$taxonomy];
    }
}
