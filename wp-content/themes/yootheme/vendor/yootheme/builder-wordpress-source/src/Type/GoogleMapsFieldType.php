<?php

namespace YOOtheme\Builder\Wordpress\Source\Type;

use YOOtheme\Builder\Source;
use YOOtheme\Str;

class GoogleMapsFieldType
{
    /**
     * @param Source $source
     *
     * @return array
     */
    public function __invoke(Source $source)
    {
        $props = [
            'address',
            'coordinates',
            'zoom',
            'place_id',
            'street_number',
            'street_name',
            'street_name_short',
            'city',
            'state',
            'state_short',
            'post_code',
            'country',
            'country_short',
        ];

        $fields = [];
        foreach ($props as $prop) {
            $fields[$prop] = [
                'type' => 'String',
                'metadata' => [
                    'label' => Str::titleCase(str_replace('_', ' ', $prop)),
                ],
            ];
        }

        $resolvers = $source->mapResolvers($this);

        return compact('fields', 'resolvers');
    }

    public function coordinates($field)
    {
        return isset($field['lat'], $field['lng']) ? "{$field['lat']},{$field['lng']}" : '';
    }
}
