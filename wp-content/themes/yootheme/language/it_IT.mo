��    1      �  C   ,      8  
   9  	   D     N     \     i     �  
   �  	   �      �  $   �     �     �     �       )   "     L     R     W     ]  E   b  W   �        a        r     w  	   �     �     �     �  %   �     �     �  F        O  M   m     �  #   �  \   �     L     S     [     p          �     �  9   �  $   �  )   	  Z  1	  
   �
     �
     �
     �
     �
     �
  
   �
  
   �
      �
  $        :     C     Q     a  $   t     �     �     �     �  A   �  Y   �     O  r   b     �     �     �     �     
          :     V     ^  K   r  .   �  Z   �     H  ,   W  f   �     �     �     �                /     8  >   I  !   �  !   �     !   -                 ,             
      "       #               )         *          1                                   +                '   %                 	   &          (       .                 /       $          0             % Comments %1$s Edit %1$s Previous %1$s at %2$s &#8592; Back to Classic Editor ,  0 Comments 1 Comment <span uk-pagination-next></span> <span uk-pagination-previous></span> Comment Comments (%s) Comments are closed. Continue reading Display your sites breadcrumb navigation. Draft Edit Email Home It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a> Name Newer Comments Next %1$s Nothing Found Older Comments Only available in Customizer. Oops! That page can&rsquo;t be found. Pages: Posted in %1$s. Ready to publish your first post? <a href="%1$s">Get started here</a>. Required fields are marked %s Save my name, email, and website in this browser for the next time I comment. Search &hellip; Search Results for &#8220;%s&#8221; Sorry, but nothing matched your search terms. Please try again with some different keywords. Title: Website Written by %s on %s. Written by %s. Written on %s. YOOtheme YOOtheme Builder You must be <a href="%s">logged in</a> to post a comment. Your comment is awaiting moderation. Your email address will not be published. Project-Id-Version: VERSION
POT-Creation-Date: 2020-02-17 16:49+0000
PO-Revision-Date: 2020-02-17 16:49+0000
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE TEAM <EMAIL@ADDRESS>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 % commenti %1$s Modifica %1$s Precedente %1$s in %2$s &#8592; Torna all'editor ,  0 commenti 1 commento <span uk-pagination-next></span> <span uk-pagination-previous></span> Commento Commenti (%s) Commenti Chiusi Continua a leggere Mostra la navigazione coi breadcrumb Bozza Modifica Email Home Sembra che non sia stato trovato nulla. Vuoi provare una ricerca? Sembra che non riusciamo a trovare cosa cerchi. Probabilmente la ricerca ti può aiutare. Lascia un commento Collegato come <a href="%1$s">%2$s</a>. <a href="%3$s" title="Scollegati dall&rsquo;account">Vuoi scollegarti?</a> Nome Nuovi Commenti Successivo %1$s Nessun risultato Vecchi Commenti Disponibile solo in Customizer. Oops! La pagina non esiste. Pagine: Pubblicato in %1$s. Sei pronto per pubblicare il tuo primo post? <a href="%1$s">Inizia qui</a>. I campi obbligatori sono contrassegnati con %s Salva il mio nome, e-mail e sito Web in questo browser per la prossima volta che commento. Cerca &hellip; Risultati della ricerca per &#8220;%s&#8221; Spiacente, ma nessun risultato corrisponde alla tua ricerca. Prova di nuovo con parole chiave diverse. Titolo: Sito web Scritto da %s il %s. Scritto da %s. Scritto il %s. YOOtheme YOOtheme Builder Devi essere <a href="%s">loggato</a> per inserire un commento. Commento in attesa di moderazione La tua email non sarà pubblicata 