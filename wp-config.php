<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'metmuseumph' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|5d69,T:]Goy((dCKxu1ebK+qh ~OftS>Mlj$ibG$p<~zgEyPKGKRi[ h1@Mn+bN' );
define( 'SECURE_AUTH_KEY',  '-Zdvr>:^vRe]FjI$?Km|K>s3ux13-vkjaTDQLubg]u,G=&ab*^=(Fs>x>v5HnOmb' );
define( 'LOGGED_IN_KEY',    'mc~IH%+3{|Z66bM2?Ba%/:b<QIg8LYUBXLmUWiCfCo_q/]@2i$gx%{Tq3u0l|/Zj' );
define( 'NONCE_KEY',        'OrPf?K~s.$ E=pk2W^+S SB}B=BSJZ* elqiRL68mDL>^exCTaMy679f=wCk&f7_' );
define( 'AUTH_SALT',        'Hbu^RKhY7ZI3^8ghGgY}bTeVuy%Ik8DI0mM4q|o=_OA]Z c1bYCwrc$v8q~4O,`a' );
define( 'SECURE_AUTH_SALT', 'DLXy7I0WL,aXDlVxhK3cny=_9Pwa6~, c<AlXUdGJt<H9Bzq%#~@xkS,08.JBOgd' );
define( 'LOGGED_IN_SALT',   '[}{/_, {Xzh;&g>mt5+~^Yw j94t]Mu|ZU8b?XrYo:`&-#u/~WV3|CY9q#7J,p Z' );
define( 'NONCE_SALT',       't/U~(]b=N@Jq.`D3lE$#u.wd$(/4ML(k|$L7[<pGMp@1nmRGZmA(t;Qz~V?:4>X6' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'met_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
